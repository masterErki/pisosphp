<?php  

    //incluyo la clases que necesito
    require 'models/class.noticiaModel.php';

    /**
    *   Fichero models/classModel.php
    */

    class NoticiasModel
    {
        private $elem;
        private $conn;
        

        public function __construct()
        {
            $this->elem = [];
            $this->conn = Conexion::$conn;
        }

        public function listado($num_regist_pag,$numpag=0)
        {
            $ini = $num_regist_pag * $numpag;

            $sql = "SELECT * FROM noticias LIMIT $ini,$num_regist_pag ";
            $query = $this->conn->query($sql);

            while($fila = $query->fetch_array())
            {
                $this->elem[]= new NoticiaModel($fila);
            }
            return $this->elem;
        }

        public function detalle($id)
        {
            $sql = "SELECT * FROM noticias WHERE idNot=$id";
            $query = $this->conn->query($sql);
            $fila = $query->fetch_array();
            return new NoticiaModel($fila);
        }

        public function getNumPag($num_regist_pag=2)
        {
            $sql = "SELECT * FROM noticias";
            $query = $this->conn->query($sql);
            return ceil($query->num_rows/$num_regist_pag);
        }

        //////////////////////////////////////////////////////////////////
        ///////// INSERCCION
        /////////////////////////////////////////////////////////////////

       
        public function insercion()
        {
            $tit = $_POST['titNot'];
            $ctxt = $_POST['content'];
            $autor = $_POST['autor'];
            $fec= date('Y-m-d H:i:s'); //Fecha actual en formato SQL

            

            $sql = "INSERT INTO noticias(tituloNot, contenidoNot, autorNot, fechaNot) VALUES ('$tit','$ctxt','$autor','$fec')";

            //echo $sql;

           // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                return true;
            }
            else{
                return $this->conn->error;
            }
            
        }

         public function borrar($id)
        {
            $sql = "DELETE FROM noticias WHERE idPiso=$id";

            //echo $sql;

           // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                return true;
            }
            else{
                return $this->conn->error;
            }
        }

        public function modificacion()
        {
          
            $id = $_POST['id'];
            $tit = $_POST['titulo'];
            $ctxtNot = $_POST['contentNot'];
            $autor = $_POST['autor'];
            $fecha = $_POST['fecha'];
            
           

            $sql = "UPDATE noticias SET tituloNot='$tit',contenidoNot='$ctxtNot',autorNot='$autor',fechaNot='$fecha' WHERE idNot='$id' ";

            //echo $sql;

           // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                return true;
            }
            else{
                return $this->conn->error;
            }
            
        }
            


            

            


            
         
           
    }