<?php 

    //Fichero models/class.pisomModel.php

    class NoticiaModel
    {

        private $idNoticia;
        private $titNot;
        private $ContentNot;
        private $autorNot;
        private $fechaNot;
        
     

        public function __construct($fila)
        {
            $this->idNoticia = $fila['idNot'];
            $this->titNot = $fila['tituloNot'];
            $this->ContentNot = $fila['contenidoNot'];
            $this->autorNot = $fila['autorNot'];
            $this->fechaNot = $fila['fechaNot'];
            
        }

     /*================================================== Getters and Setters   
    

    /**
     * Gets the value of idNoticia.
     *
     * @return mixed
     */
    public function getIdNoticia()
    {
        return $this->idNoticia;
    }

    /**
     * Sets the value of idNoticia.
     *
     * @param mixed $idNoticia the id noticia
     *
     * @return self
     */
    private function _setIdNoticia($idNoticia)
    {
        $this->idNoticia = $idNoticia;

        return $this;
    }

    /**
     * Gets the value of titNot.
     *
     * @return mixed
     */
    public function getTitNot()
    {
        return $this->titNot;
    }

    /**
     * Sets the value of titNot.
     *
     * @param mixed $titNot the tit not
     *
     * @return self
     */
    private function _setTitNot($titNot)
    {
        $this->titNot = $titNot;

        return $this;
    }

    /**
     * Gets the value of ContentNot.
     *
     * @return mixed
     */
    public function getContentNot()
    {
        return $this->ContentNot;
    }

    /**
     * Sets the value of ContentNot.
     *
     * @param mixed $ContentNot the content not
     *
     * @return self
     */
    private function _setContentNot($ContentNot)
    {
        $this->ContentNot = $ContentNot;

        return $this;
    }

    /**
     * Gets the value of autorNot.
     *
     * @return mixed
     */
    public function getAutorNot()
    {
        return $this->autorNot;
    }

    /**
     * Sets the value of autorNot.
     *
     * @param mixed $autorNot the autor not
     *
     * @return self
     */
    private function _setAutorNot($autorNot)
    {
        $this->autorNot = $autorNot;

        return $this;
    }

    /**
     * Gets the value of fechaNot.
     *
     * @return mixed
     */
    public function getFechaNot()
    {
        return $this->fechaNot;
    }

    /**
     * Sets the value of fechaNot.
     *
     * @param mixed $fechaNot the fecha not
     *
     * @return self
     */
    private function _setFechaNot($fechaNot)
    {
        $this->fechaNot = $fechaNot;

        return $this;
    }
}