<?php  

    //incluyo la clases que necesito
    require 'models/class.pisoModel.php';

    /**
    *   Fichero models/classModel.php
    */

    class PisosModel
    {
        private $elem;
        private $conn;
        

        public function __construct()
        {
            $this->elem = [];
            $this->conn = Conexion::$conn;
        }

        public function listado($num_regist_pag,$numpag=0)
        {
            $ini = $num_regist_pag * $numpag;

            $sql = "SELECT * FROM pisos LIMIT $ini,$num_regist_pag ";
            $query = $this->conn->query($sql);

            while($fila = $query->fetch_array())
            {
                $this->elem[]= new PisoModel($fila);
            }
            return $this->elem;
        }

        public function detalle($id)
        {
            $sql = "SELECT * FROM pisos WHERE idPiso=$id";
            $query = $this->conn->query($sql);
            $fila = $query->fetch_array();
            return new PisoModel($fila);
            
        }

        public function getNumPag($num_regist_pag=2)
        {
            $sql = "SELECT * FROM pisos";
            $query = $this->conn->query($sql);
            return ceil($query->num_rows/$num_regist_pag);
        }

        //////////////////////////////////////////////////////////////////
        ///////// INSERCCION
        /////////////////////////////////////////////////////////////////

       
        public function insercion()
        {
            $dir = $_POST['dire'];
            $car = $_POST['carater'];
            $precio = $_POST['precio'];
            $ciudad = $_POST['ciudad'];

            if(is_uploaded_file( $_FILES['imgPiso']['tmp_name']))
            {
                $img =time().$_FILES['imgPiso']['name'];
                move_uploaded_file( $_FILES['imgPiso']['tmp_name'],'img/'.$img);               
            }
            else{
                $img = 'nodisponible.png';
            }


            //echo $dir.' / '.$car.' / '.$img.' / '.$precio.' / '.$ciudad;

            $sql = "INSERT INTO pisos(direccionPiso, caracteristicasPiso, imagenPiso, precioPiso, ciudadPiso) VALUES ('$dir','$car','$img','$precio','$ciudad')";

            //echo $sql;

           // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                return true;
            }
            else{
                return $this->conn->error;
            }
            
        }

         public function borrar($id)
        {
            $sql = "DELETE FROM pisos WHERE idPiso=$id";

            //echo $sql;

           // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                return true;
            }
            else{
                return $this->conn->error;
            }
        }

        public function modificacion()
        {
            $id = $_POST['id'];
            $dir = $_POST['dire'];
            $car = $_POST['carater'];
            $precio = $_POST['precio'];
            $ciudad = $_POST['ciudad'];
            

            if(is_uploaded_file( $_FILES['imgPiso']['tmp_name']))
            {
                $img =time().$_FILES['imgPiso']['name'];
                move_uploaded_file( $_FILES['imgPiso']['tmp_name'],'img/'.$img);               
            }
            else{
                $img = $_POST['img'];
            }


            //echo $dir.' / '.$car.' / '.$img.' / '.$precio.' / '.$ciudad;
           

            $sql = "UPDATE pisos SET direccionPiso='$dir', caracteristicasPiso='$car', imagenPiso='$img', precioPiso='$precio', ciudadPiso='$ciudad' WHERE idPiso=$id ";

            //echo $sql;

           // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                return true;
            }
            else{
                return $this->conn->error;
            }
            
        }
            


            

            


            
         
           
    }