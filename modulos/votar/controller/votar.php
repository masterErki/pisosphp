<?php 

    require_once 'modulos/votar/model/votarModel.php';

    $votos = new VotarModel();

    global $e;

    if(isset($_GET['accionVotar'])){
        $e->getIdPiso()==$_GET['idPiso'];
        $votos->votar($_GET['idUsu'],$_GET['idPiso'],$_GET['accionVotar']);
    }

    // extraemos la media redondeada de votos de bbdd
    $estrellas =round($votos->estrellas($e));

    require 'modulos/votar/view/votacionView.php';
?>