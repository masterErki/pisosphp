<?php 

    //Fichero modulos/menu/model/menuModel.php
    //llamamos a nuestro modelo de item que sera un elemento del menu
    require 'modulos/comentario/model/comentarioModel.php';

    
    class ComentariosModel
    {
        
        private $elem;
        private $conn;
        

        public function __construct()
        {
            $this->elem = [];
            $this->conn = Conexion::$conn;
        }

        public function listado($idPiso)
        {
            

            $sql = "SELECT *,usuarios.nombreUsu FROM comentarios INNER JOIN usuarios ON comentarios.idUsu=usuarios.idUsu WHERE idPiso =$idPiso";
            // $sql = "SELECT * FROM comentarios WHERE idPiso=$idPiso";
            $query = $this->conn->query($sql);

            while($fila = $query->fetch_array())
            {
                

                $this->elem[]=[$fila['nombreUsu'] => new ComentarioModel($fila)];
            }
            return $this->elem;
        }

         public function insercion($tituloCom, $descripccionCom,$idUser,$idPiso)
        {
            
            $fec= date('Y-m-d H:i:s'); //Fecha actual en formato SQL
            

            $sql = "INSERT INTO comentarios(tituloCom, descripccionCom, idUsu, idPiso, fechaCom) VALUES ('$tituloCom','$descripccionCom',$idUser,$idPiso,'$fec') ";

            echo $sql;

           // Devuelve un objeto de la class Mysqli_result
            $query = $this->conn->query($sql);

            if($query){
                return true;
            }
            else{
                return $this->conn->error;
            }
            
        }

        public function borrar($idCom){
            $sql = "DELETE FROM comentarios WHERE idCom=$idCom";
            $query = $this->conn->query($sql);
        }

        public function modificar($idCom)
        {
            $sql = "SELECT * FROM comentarios WHERE idCom=$idCom";
            $query = $this->conn->query($sql);
            return new ComentarioModel($query->fetch_array());
        }

        public function modificacion($tit,$txtCom,$idCom)
        {
            $sql = "UPDATE comentarios SET tituloCom='$tit',descripccionCom='$txtCom' WHERE idCom=$idCom";
            $query = $this->conn->query($sql);

            if($query === TRUE){
                return TRUE;
            }
            else{
                //return $this->conn->error;
                return FALSE;
            }
        }
    }
    



?>

