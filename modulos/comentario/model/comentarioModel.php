<?php 

    //Fichero modulos/comentario/model/comentarioModel.php

    class ComentarioModel
    {
        private $idCom;
        private $tituloCom;
        private $descripccionCom;
        private $idUsu;
        private $idPiso;
        private $fechaCom;
        public $esEditable= false;
        public $esBorrable= false;


        public function __construct($fila)
        {
            $this->idCom=$fila['idCom'];
            $this->tituloCom=$fila['tituloCom'];
            $this->descripccionCom=$fila['descripccionCom'];
            $this->idUsu=$fila['idUsu'];
            $this->idPiso=$fila['idPiso'];
            $this->fechaCom=$fila['fechaCom'];
            
            if($_SESSION['usuarioConectado']['tipoUsu']=='admin'){
                $this->esBorrable = true;
            }
            if($_SESSION['usuarioConectado']['idUsu']==$fila['idUsu']){
                $this->esBorrable = true;
                $this->esEditable = true;
            }
        }
        
        /**********************************************************/
        /**
         * Gets the value of idCom.
         *
         * @return mixed
         */
        public function getIdCom()
        {
            return $this->idCom;
        }
    
        /**
         * Gets the value of tituloCom.
         *
         * @return mixed
         */
        public function getTituloCom()
        {
            return $this->tituloCom;
        }
    
        /**
         * Gets the value of descripccionCom.
         *
         * @return mixed
         */
        public function getDescripccionCom()
        {
            return $this->descripccionCom;
        }
    
        /**
         * Gets the value of idUsu.
         *
         * @return mixed
         */
        public function getIdUsu()
        {
            return $this->idUsu;
        }
    
        /**
         * Gets the value of idPiso.
         *
         * @return mixed
         */
        public function getIdPiso()
        {
            return $this->idPiso;
        }
    
        /**
         * Gets the value of fechaCom.
         *
         * @return mixed
         */
        public function getFechaCom()
        {
            return $this->fechaCom;
        }
    }



?>



