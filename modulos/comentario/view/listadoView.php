
<?php 
//echo "<pre></pre>";
//print_r($comentarios); 
//echo "</pre>";


?>

<?php 
  if(isset($_SESSION['usuarioConectado'])){ 
    echo Form::a('index.php?controller=pisosController.php&accion=detalle&id='.$_GET['id'].'&com=insertar',Form::btn_HTML5('button','Insertar comentario',['class'=>'btn btn-default']));
  } 
?>

<?php foreach ($comentarios as $com ){ ?>
  <?php foreach ($com as $cm => $v ){ ?>

  <?php //print_r($cm); ?>
  <?php //echo $com[0]; ?>


  <section class="clearfix">
      <header>
        <h1><?php echo $v->getTituloCom(); ?>
          <!--  si es el usuario  pueda borrar o pueda modificar comentario -->
          <small>
          
           <?php if($v->esEditable){ ?> 
           - 
            <?php echo Form::a('index.php?controller=pisosController.php&accion=detalle&id='.$_GET['id'].'&com=modificar&idCom='.$v->getIdCom(),'Modificar');
            ?>
          <?php } ?>
          
          
           <?php if($v->esBorrable){ ?> 
           - 
            <?php 
                echo Form::a("index.php?controller=pisosController.php&accion=detalle&id=".$_GET['id']."&com=borrar&idCom=".$v->getIdUsu(),'Borrar',['onclick'=>"if(!confirm(\" Estas seguro\")){return false;}"]); 
              ?> 
          <?php } ?>
            
          </small>

        </h1>
      </header>
      <article><?php echo $v->getDescripccionCom(); ?></article>
      <footer class="well col-md-4 pull-right"><?php echo $v->getFechaCom(); ?> - <b> <?php echo $cm; ?></b></footer>
  </section>

  <!-- damos opcion para insertar comentario -->

  <hr><hr>

  <?php } ?>
<?php } ?>

