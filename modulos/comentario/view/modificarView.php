
<?php 


  /* insertar formulario */


  echo Form::ini_form([
      'action'=>'index.php?controller=pisosController.php&accion=detalle&id='.$_GET['id'].'&com=modificacion',
      'method'=>'POST',
      //'enctype' =>"multipart/form-data",
      'role'=>'form',
      'class' =>'form-horizontal col-md-8'
  ]);

  echo Form::input('text','titulo','Titulo comentario',$comMod->getTituloCom(),['class'=>'form-control','placeholder'=>'Introduce titulo del comentario']);
  echo Form::txt_area('content','Comentario',$comMod->getDescripccionCom(),[
      'cols'=> '15',
      'rows'=>'5',
      'placeholder'=>'Introduce descripcion del piso',
      'class'=>'form-control'
  ]);
  
  echo Form::input('hidden','idCom','',$comMod->getIdCom());
  

  echo Form::btn_HTML5('submit','Insertar',['class'=> 'btn btn-primary pull-right','style'=>'margin-bottom:15px;']);


  echo Form::fnal_form();