<?php 

    require 'modulos/login/model/usuarioModel.php';

    // Creamo el objeto login
    $login = new UsuarioModel();
   
   if (!isset($_SESSION['usuarioConectado'])) 
   {
        $_SESSION['usuarioConectado'] = NULL;
   }

   if(isset($_COOKIE['matener'])){
    $_SESSION['usuarioConectado']= $login->comprobarCookie($_COOKIE['matener']);
   }

   if(isset($_POST['conectar']))
   {
        $_SESSION['usuarioConectado'] = $login->comprobar($_POST['user'],$_POST['clave']);
        if(isset($_POST['mantener'])){
            setcookie('mantener',md5($_SESSION['usuarioConectado']['correoUsu']), time()+(60*60*24));
        }
   }

   if(isset($_GET['desconectar'])){
    $_SESSION['usuarioConectado'] = NULL;
    setcookie('mantener','',0);
   }

    
    
    // Pedimos la vista para ver login
    require 'modulos/login/view/loginView.php';
?>


