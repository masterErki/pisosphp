<?php 
    // Fichero modulos/menu/view/menuView.php

    if(isset($_SESSION['usuarioConectado']))
    {
        echo 'Bienvenido '.$_SESSION['usuarioConectado']['correoUsu'].' - ';

        echo Form::a('index.php?desconectar=si',Form::btn_HTML5('button','Desconectar',['name'=>'desconectar','class'=>'btn btn-info']));
    }
    else
    {
        // Genero formulario login

        echo Form::ini_form([
            'action' => 'index.php',
            'method' => 'POST',
            'class'=>"form-inline",
            'role'=>"form"
        ]);

        echo Form::input('text','user','Nombre:','',['placeholder'=>'Introduce nombre de usuario']);
        echo Form::input('password','clave','Contraseña:','',['placeholder'=>'Introduce clave de usuario']);
        echo Form::ckeckbox('mantener','Mantener',true);
        echo Form::btn_HTML5('submit','Login',['name'=>'conectar','class'=>'btn btn-info']);

        echo Form::fnal_form();
    }

?>



  