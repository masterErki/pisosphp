<?php 

    //Fichero modulos/menu/model/menuModel.php

    //llamamos a nuestro modelo de item que sera un elemento del menu
    require 'modulos/banner/model/itemBannerModel.php';

    //Declaro el nombre de la clase
    class BannerModel
    {
        private $items=[];

        public function add($enlace)
        {
            $item = new itemsBannerModel($enlace);
            $this->items[] = $item;
        }

        public function devolver()
        {
            
            return $this->items;
        }

        public function devolver_Aleatorio()
        {
            $num = rand(0,count($this->items)-1);
            return $this->items[$num];
        }
    }



?>



