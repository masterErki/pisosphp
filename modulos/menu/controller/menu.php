<?php 

    require 'modulos/menu/model/menuModel.php';


    $menu = new MenuModel();
    $menu->add('index.php','Ir a inicio');
    $menu->add("index.php?controller=pisosController.php&accion=insertar","Alta Piso");
    $menu->add("index.php?controller=noticiasController.php&accion=listado","Noticias de Pisos");
    $menu->add('index.php?controller=pisosController.php&accion=rss','RSS');
    $menu->add('index.php?controller=pisosController.php&accion=rss-externo','RRS Comunity');
    
    $items = $menu->devolcer();

    require 'modulos/menu/view/menuView.php';
?>