<?php 

    //Fichero modulos/menu/model/menuModel.php
   

    class itemMenuModel
    {
        private $enlace;
        private $titulo;

        public function __construct($enlace,$titulo)
        {
            $this->enlace = $enlace;
            $this->titulo = $titulo;

        }
    
    /**
     * Gets the value of enlace.
     *
     * @return mixed
     */
        public function getEnlace()
        {
            return $this->enlace;
        }

    /**
     * Gets the value of titulo.
     *
     * @return mixed
     */
        public function getTitulo()
        {
            return $this->titulo;
        }
}
    



?>