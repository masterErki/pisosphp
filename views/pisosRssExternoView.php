<?php require 'views/encabezadoViews.php'; ?>
    

    <div class="row">
        <!-- migas de pan -->
      <?php echo Helper::migas_pan(['Inicio'=>'index.php?controller=pisosController.php&accion=listado','RSS COMUNITY'=>'index.php?controller=pisosController.php&accion=rss-externo']); ?>
        <section class="col-md-12">

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Titulo</th>
                        <th>Fecha Publicacion</th>
                        <th>Descripcion</th>
                        <th>Link</th>
                    </tr>

                </thead>
                <tbody>
                    <?php foreach($elem as $e){  
                            foreach ($e as $p) {?>

                                <tr>
                                    <td><?php echo $p->title; ?></td>
                                    <td><?php echo $p->pubDate; ?></td>
                                    <td><?php echo $p->description; ?></td>
                                    <td><a href="<?php echo $p->link; ?>"><?php echo $p->title; ?> -<?php echo $p->pubDate; ?> </a></td>
                                </tr>
                            <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </section>
    </div>



 <?php require 'views/pieViews.php'; ?>


 