    </div>

    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function(){
            //alert('pisos');
            
            // Activate Carousel
            $("#myCarousel").carousel(
                interval: 3000
            );

            // Enable Carousel Indicators
            // $(".item").click(function(){
            //     $("#myCarousel").carousel(1);
            // });

            // Enable Carousel Controls
            // $(".left").click(function(){
            //     $("#myCarousel").carousel("prev");
            // });
        });
    </script>

    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script> 
    <!-- Just be careful that you give correct path to your tinymce.min.js file, above is the default example -->
    <script>
        tinymce.init({selector:'textarea'});
    </script>
  </body>
</html>