<?php require 'views/encabezadoViews.php'; ?>
    
    <div class="row">
      <section class="col-sm-12"> 
      <?php //foreach ($elem as $e): ?>
      <!-- migas de pan -->
      <?php echo Helper::migas_pan(['Inicio'=>'index.php?controller=pisosController.php&accion=listado','Detalle'=>'index.php?controller=pisosController.php&accion=detalle&id='.$e->getIdPiso()]) ?>
        
        <article>
          <header>
                <h2><?php echo $e->getDireccionPiso(); ?>
                  - <small><?php echo $e->getCiudadPiso(); ?></small>
                  
                  - <button type="button" class="btn btn-default" aria-label="Left Align">
                      <span class="glyphicon glyphicon-remove" aria-hidden="true"><?php echo Form::a("index.php?controller=pisosController.php&accion=borrar&id=".$e->getIdPiso(),'Borrar',['onclick'=>"if(!confirm(\" Estas seguro\")){return false;}"]); ?></span>
                    </button>

                  
                </h2>
              
          </header>

          <section class="col-sm-12 ">
            <div class="row">
              <div class="col-sm-3">
                <img src="img/<?php echo $e->getImagenPiso(); ?>" width="200" style="float:left;">
              </div>
              <div class="col-sm-9">
                <?php echo $e->getCaracteristicasPiso(); ?>
              </div> 
                
            </div>
          </section>

          <footer class="well col-sm-3 col-sm-offset-9" style="text-align: right;color: #e95420; background: whitesmoke;"><?php echo $e->getPrecioPiso() ?> Euros</footer>
        </article>
      <?php //endforeach; ?>
      </section>
      <div class="col-md-8 col-md-offset-3 ">
        <?php cargarModulo('comentario'); ?>        
      </div>
    
    </div>
    
<?php require 'views/pieViews.php'; ?>
