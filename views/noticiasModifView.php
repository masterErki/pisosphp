<?php require 'views/encabezadoViews.php'; ?>
    
    <div class="row">
      <section class="col-sm-12"> 
      <?php //foreach ($elem as $e){ ?>
      <!-- migas de pan -->
        <?php echo Helper::migas_pan([
          'Inicio'=>'index.php?controller=pisosController.php&accion=listado',
          'Modificar'=>'index.php?controller=pisosController.php&accion=modificar&id='.$e->getIdNoticia()
          ]); 
        ?>
        <article>
          

          <section class="col-sm-12 ">
            <?php  
            //echo Form::a('index.php?controller=pisosController.php&accion=listado','Inicio');
            echo Form::ini_form([
                'action'=>'index.php?controller=noticiasController.php&accion=modifica',
                'method'=>'POST',
                //'enctype' =>"multipart/form-data",
                'role'=>'form',
                'class' =>'form-horizontal col-md-8'
            ]);

            echo Form::input('text','titulo','Titutlo Noticia',$e->getTitNot(),['class'=>'form-control','placeholder'=>'Introduce el titulo de la noticia']);
            echo Form::txt_area('contentNot','Contenido de la noticia',$e->getContentNot(),[
                'cols'=> '15',
                'rows'=>'5',
                'placeholder'=>'Introduce descripcion del piso',
                'class'=>'form-control'
            ]);
            echo Form::input('text','autor','Autor',$e->getAutorNot(),['class'=>'form-control','placeholder'=>'Introduce el autor']);
            echo Form::input('text','fecha','Fecha',$e->getFechaNot(),['class'=>'form-control','placeholder'=>'Introduce la fecha']);
            
            echo Form::input('hidden','id','',$e->getIdNoticia());
            

            echo Form::btn_HTML5('submit','Actualizar',['class'=> 'btn btn-primary pull-right','style'=>'margin-bottom:15px;']);

            echo Form::fnal_form();

            
        ?>
          </section>

          <!-- <footer class="well col-sm-3 col-sm-offset-9" style="text-align: right;color:red; background: blue;"><?php //echo $e->getPrecioPiso() ?> Euros</footer> -->
        </article>
      <?php //} ?>
      </section>
    
    </div>
    
<?php require 'views/pieViews.php'; ?>
