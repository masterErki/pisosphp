<?php require 'views/encabezadoViews.php'; ?>
    
    
    <div class="row">
      <?php echo Helper::migas_pan(['inicio'=>'index.php?controller=pisosController.php&accion=listado']) ?>

      <!-- <nav class="col-md-3">
          AUI VENDRA MENU
          
          <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="index.php?controller=pisosController.php&accion=insertar">Alta Piso</a></li>
            <li><a href="index.php?controller=pisosController.php&accion=rss">RSS</a></li>
            <li><a href="index.php?controller=pisosController.php&accion=rss-externo">RSS COMUNITY</a></li>
          </ul>
          
      </nav> -->

      <!-- Menu desde la funcion -->
      <?php cargarModulo('menu'); ?>

      <!-- Creacion de banner -->
      <section>
        <?php cargarModulo('banner'); ?>
      </section>
        

      <section class="col-md-9"> 
        <?php foreach ($elem as $e): ?>
          <article class="">

            <header>
                <a href="index.php?controller=pisosController.php&accion=detalle&id=<?php echo $e->getIdPiso(); ?>">
                  <h3><?php echo $e->getDireccionPiso(); ?>
                    - <small><?php echo $e->getCiudadPiso(); ?></small>
                    <!-- si existe usuario  -->
                    <?php if(isset($_SESSION['usuarioConectado'])): ?>
                      - <small><?php echo Form::a("index.php?controller=pisosController.php&accion=borrar&id=".$e->getIdPiso(),'Borrar',['onclick'=>"if(!confirm(\" Estas seguro\")){return false;}"]); ?></small>
                      - <small>
                        <?php echo Form::a('index.php?controller=pisosController.php&accion=modificar&id='.$e->getIdPiso(),'Modificar') ?>
                      </small>
                    <?php endif; ?>
                    <!-- /si existe usuario  -->
                  </h3>
                </a>
            </header>

            <section class="col-sm-12 ">
              
                <div class="col-md-2">
                  <img src="img/<?php echo $e->getImagenPiso(); ?>" width="130" style="float:left;margin-bottom: 15px;">
                  <!-- Carga modulo votacion -->
                  
                    <div class="caption">
                      <?php cargarModulo('votar'); ?>
                    </div>
                  
                </div>
                  
                <div class="col-md-10 ">
                  <?php echo $e->getCaracteristicasPiso(); ?>
                </div> 
              
            </section>
                  
            <footer class="well col-md-3 col-md-offset-9" style="text-align: right;color: #e95420; background: whitesmoke;"><?php echo $e->getPrecioPiso() ?> Euros</footer>

          </article>
        <?php endforeach; ?>    
        <!-- Paginacion -->

        <?php echo Helper::pagination($numpag,$numPag,'index.php?controller=pisosController.php&numpag='); ?>

      </section>
    
    </div>
    
<?php require 'views/pieViews.php'; ?>
