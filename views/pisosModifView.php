<?php require 'views/encabezadoViews.php'; ?>
    
    <div class="row">
      <section class="col-sm-12"> 
      <?php //foreach ($elem as $e): ?>
      <!-- migas de pan -->
      <?php echo Helper::migas_pan(['Inicio'=>'index.php?controller=pisosController.php&accion=listado','Modificar'=>'index.php?controller=pisosController.php&accion=modificar&id='.$e->getIdPiso()]) ?>
        <article>
          

          <section class="col-sm-12 ">
            <?php  
            //echo Form::a('index.php?controller=pisosController.php&accion=listado','Inicio');
            echo Form::ini_form([
                'action'=>'index.php?controller=pisosController.php&accion=modifica',
                'method'=>'POST',
                'enctype' =>"multipart/form-data",
                'role'=>'form',
                'class' =>'form-horizontal col-md-8'
            ]);

            echo Form::input('text','dire','Direccion del Piso',$e->getDireccionPiso(),['class'=>'form-control','placeholder'=>'Introduce la direccion del piso']);
            echo Form::txt_area('carater','Caracteristicas',$e->getCaracteristicasPiso(),[
                'cols'=> '15',
                'rows'=>'5',
                'placeholder'=>'Introduce descripcion del piso',
                'class'=>'form-control'
            ]);
            echo Form::input('text','precio','Precio',$e->getPrecioPiso(),['class'=>'form-control','placeholder'=>'Introduce el precio en euros']);
            echo Form::input('text','ciudad','Ciudad',$e->getCiudadPiso(),['class'=>'form-control','placeholder'=>'Introduce la ciudad']);
            echo Form::input('file','imgPiso','Imagen','',['class'=>'form-control','placeholder'=>'Introduce la Imagen']);
            echo Form::input('hidden','id','',$e->getIdPiso());
            echo Form::input('hidden','img','',$e->getImagenPiso());

            echo Form::btn_HTML5('submit','Actualizar',['class'=> 'btn btn-primary pull-right','style'=>'margin-bottom:15px;']);

            echo Form::fnal_form();

            echo Helper::img($e->getImagenPiso(),['class'=>'col-md-4']);
        ?>
          </section>

          <!-- <footer class="well col-sm-3 col-sm-offset-9" style="text-align: right;color:red; background: blue;"><?php echo $e->getPrecioPiso() ?> Euros</footer> -->
        </article>
      <?php //endforeach; ?>
      </section>
    
    </div>
    
<?php require 'views/pieViews.php'; ?>
