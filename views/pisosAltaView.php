<?php require 'views/encabezadoViews.php'; ?>
    
    <div class="row">
      <!-- Migas de pan -->
      <?php echo Helper::migas_pan(['inicio'=>'index.php?controller=pisosController.php&accion=listado','Alta de pisos'=>'index.php?controller=pisosController.php&accion=insertar']) ?>
      <nav class="col-md-3">
          <!-- AUI VENDRA MENU -->
          
          <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="index.php?controller=pisosController.php&accion=insertar">Alta Piso</a></li>
            <li><a href="#">Menu 2</a></li>
            <li><a href="#">Menu 3</a></li>
          </ul>
          
      </nav>
      <section class="col-md-9">
        <?php  
            //echo Form::a('index.php?controller=pisosController.php&accion=listado','Inicio');
            echo Form::ini_form([
                'action'=>'index.php?controller=pisosController.php&accion=insercion',
                'method'=>'POST',
                'enctype' =>"multipart/form-data",
                'role'=>'form',
                'class' =>'form-horizontal col-md-8'
            ]);

            echo Form::input('text','dire','Direccion del Piso','',['class'=>'form-control','placeholder'=>'Introduce la direccion del piso']);
            echo Form::txt_area('carater','Caracteristicas','',[
                'cols'=> '15',
                'rows'=>'5',
                'placeholder'=>'Introduce descripcion del piso',
                'class'=>'form-control'
            ]);
            echo Form::input('text','precio','Precio','',['class'=>'form-control','placeholder'=>'Introduce el precio en euros']);
            echo Form::input('text','ciudad','Ciudad','',['class'=>'form-control','placeholder'=>'Introduce la ciudad']);
            echo Form::input('file','imgPiso','Imagen','',['class'=>'form-control','placeholder'=>'Introduce la Imagen']);

            echo Form::btn_HTML5('submit','Insertar',['class'=> 'btn btn-primary pull-right','style'=>'margin-bottom:15px;']);


            echo Form::fnal_form();
        ?>
      </section>
    
    </div>
    
<?php require 'views/pieViews.php'; ?>
