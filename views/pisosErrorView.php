<?php require 'views/encabezadoViews.php'; ?>
    
    <?php if($_GET['controller'] == 'pisosController') : ?>
      <div class="row">
        <!-- Migas de pan -->
        <?php echo Helper::migas_pan(['Inicio'=>'index.php?controller=pisosController.php&accion=listado']); ?>
        <!-- Insertar menu error -->

        <section class="col-md-9">
          
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error:!!!</strong> <?php echo $error; ?>
          </div>
        </section>
      
      </div>
    <?php else: ?>
      <div class="row">
        <!-- Migas de pan -->
        <?php echo Helper::migas_pan(['Inicio'=>'index.php?controller=noticiasController.php&accion=listado']) ?>
        <!-- Insertar menu error -->

        <section class="col-md-9">
          
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>Error:!!!</strong> <?php echo $error; ?>
          </div>
        </section>
      
      </div>
    <?php endif; ?>
    
<?php require 'views/pieViews.php'; ?>
