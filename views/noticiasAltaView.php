<?php require 'views/encabezadoViews.php'; ?>
    
    <div class="row">
      <!-- Migas de pan -->
      <?php echo Helper::migas_pan(['Inicio'=>'index.php','Noticias'=>'index.php?controller=noticiasController.php&accion=listado','Alta de Noticia'=>'index.php?controller=noticiasController.php&accion=insertar']) ?>
      <nav class="col-md-3">
          <!-- AUI VENDRA MENU -->
          
          
          
      </nav>
      <section class="col-md-9">
        <?php  
            //echo Form::a('index.php?controller=pisosController.php&accion=listado','Inicio');
            echo Form::ini_form([
                'action'=>'index.php?controller=noticiasController.php&accion=insercion',
                'method'=>'POST',
                //'enctype' =>"multipart/form-data",
                'role'=>'form',
                'class' =>'form-horizontal col-md-8'
            ]);

            echo Form::input('text','titNot','Titulo de la noticia','',['class'=>'form-control','placeholder'=>'Introduce el titulo de la noticia']);
            echo Form::txt_area('content','Contenido de la noticia','',[
                'cols'=> '15',
                'rows'=>'5',
                'placeholder'=>'Introduce el contenido de la noticia',
                'class'=>'form-control'
            ]);
            echo Form::input('text','autor','Autor de la noticia','',['class'=>'form-control','placeholder'=>'Introduce el autor de la noticia']);
            

            echo Form::btn_HTML5('submit','Insertar',['class'=> 'btn btn-primary pull-right','style'=>'margin-bottom:15px;']);


            echo Form::fnal_form();
        ?>
      </section>
    
    </div>
    
<?php require 'views/pieViews.php'; ?>
