<?php require 'views/encabezadoViews.php'; ?>
    
    <div class="row">
      <section class="col-sm-12"> 
      <?php //foreach ($elem as $e): ?>
      <!-- migas de pan -->
      <?php echo Helper::migas_pan([
        'Inicio'=>'index.php',
        'Noticias' => 'index.php?controller=noticiasp0Controller.php&accion=listado',
        'Detalle'=>'index.php?controller=pisosController.php&accion=detalle&id='.$e->getIdNoticia()]) ?>
        <article>
          <header>
                <h2><?php echo $e->getTitNot(); ?>
                  - <small><?php echo $e->getAutorNot(); ?></small>
                  
                  - <button type="button" class="btn btn-default" aria-label="Left Align">
                      <span class="glyphicon glyphicon-remove" aria-hidden="true"><?php echo Form::a("index.php?controller=pisosController.php&accion=borrar&id=".$e->getIdNoticia(),'Borrar',['onclick'=>"if(!confirm(\" Estas seguro\")){return false;}"]); ?></span>
                    </button>

                  
                </h2>
              
          </header>

          <section class="col-sm-12 ">
            <div class="row">
              
              <div class="col-sm-9">
                <?php echo $e->getContentNot(); ?>
              </div> 
                
            </div>
          </section>

          <footer class="well col-sm-3 col-sm-offset-9" style="text-align: right;"><?php echo $e->getAutorNot().' - '.$e->getFechaNot() ?> </footer>
        </article>
      <?php //endforeach; ?>
      </section>
    
    </div>
    
<?php require 'views/pieViews.php'; ?>
