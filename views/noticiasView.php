<?php require 'views/encabezadoViews.php'; ?>
    
    
    <div class="row">
      <?php echo Helper::migas_pan(['Inicio'=>'index.php?controller=pisosController.php&accion=listado','Noticias'=>'index.php?controller=noticiasController.php&accion=listado']);?>

      <!-- <nav class="col-md-3">
          AUI VENDRA MENU
          
          <ul class="nav nav-pills nav-stacked">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="index.php?controller=pisosController.php&accion=insertar">Alta Piso</a></li>
            <li><a href="index.php?controller=pisosController.php&accion=rss">RSS</a></li>
            <li><a href="index.php?controller=pisosController.php&accion=rss-externo">RSS COMUNITY</a></li>
          </ul>
          
      </nav> -->

      <!-- Menu desde la funcion -->
      <?php cargarModulo('menu','menu_not'); ?>

      <!-- Creacion de banner -->
      <section>
        <?php //cargarModulo('banner'); ?>
      </section>
        

      <section class="col-md-9"> 
        <?php foreach ($elem as $e): ?>
          <article class="">

            <header>
                <a href="index.php?controller=noticiasController.php&accion=detalle&id=<?php echo $e->getIdNoticia(); ?>">
                  <h3><?php echo $e->getTitNot(); ?>
                    <!-- si existe usuario  -->
                    <?php if(isset($_SESSION['usuarioConectado'])): ?>
                      - <small><?php echo Form::a("index.php?controller=noticiasController.php&accion=borrar&id=".$e->getIdNoticia(),'Borrar',['onclick'=>"if(!confirm(\" Estas seguro\")){return false;}"]); ?></small>
                      - <small>
                        <?php echo Form::a('index.php?controller=noticiasController.php&accion=modificar&id='.$e->getIdNoticia(),'Modificar') ?>
                      </small>
                    <?php endif; ?>

                  </h3>
                </a>
            </header>

            <section class="col-sm-12 ">
                <div class="col-md-10 ">
                  <?php echo $e->getContentNot(); ?>
                </div> 
            </section>
              
            <footer class="well col-md-3 col-md-offset-9" style="text-align: right;color: #e95420; background: whitesmoke;">
              <?php echo $e->getAutorNot().' - '.$e->getFechaNot() ?>
            </footer>
          </article>
                
        <?php endforeach; ?>    
              
        <!-- Paginacion -->
        <?php echo Helper::pagination($numpag,$numPag,'index.php?controller=noticiasController.php&numpag='); ?>
                  
      </section>
                



    
    </div>
    
<?php require 'views/pieViews.php'; ?>
