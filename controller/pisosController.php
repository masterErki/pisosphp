<?php 

require 'models/class.pisosModel.php';
require 'models/class.rssModel.php';

$pisos = new PisosModel();

//Evaluamos la accion a realizar
if(isset($_GET['accion'])){
    $accion = $_GET['accion'];
}
else
{
    $accion = 'listado';
}

// Acciones a determinar
switch ($accion) {
    case 'detalle':
        $id=$_GET['id'];
        $e = $pisos->detalle($id);
        //llamo a la vista 
        require('views/pisosDetalleView.php');
        break;
    case 'borrar':
        $id = $_GET['id'];
        $consult =$pisos->borrar($id);
        if($consult===true)
        {
            // return 'Insercion con exito';
            header('location:index.php?controller=pisosController.php&accion=listado');
        }
        else
        {
            $error=$consulta;
            require 'views/pisosErrorView.php';
        }
        break;

    case 'insertar':
        require('views/pisosAltaView.php');
        break;
    
    case 'insercion':
        $consult =$pisos->insercion(); 
        if($consult===true)
        {
            // return 'Insercion con exito';
            header('location:index.php?controller=pisosController.php&accion=listado');
        }
        else
        {
            $error=$consulta;
            require 'views/pisosErrorView.php';
        }

        break;
            
    case 'modificar':
        $id=$_GET['id'];
        $e = $pisos->detalle($id);
        //llamo a la vista 
        require('views/pisosModifView.php');
        break;

    case 'modifica':
        $consult =$pisos->modificacion(); 
        if($consult===true)
        {
            // return 'Insercion con exito';
            header('location:index.php?controller=pisosController.php&accion=listado');
        }
        else
        {
            $error=$consulta;
            require 'views/pisosErrorView.php';
        }

        break;

    case 'rss':
        $elem =$pisos->listado(10,0);
        require('views/pisosRssView.php');
        break;

    case 'rss-externo':
        // Me creo un array vacio
        $elem = [];
        $urls =[
            'http://192.168.1.58/datw/David/pisosphp/index.php?controller=pisosController.php&accion=rss',
            'http://192.168.1.59/pisosphp/index.php?controller=pisosController.php&accion=rss',
            'http://192.168.1.54/aitor/20170621/pisosphp/index.php?controller=pisosController.php&accion=rss',
            'http://192.168.1.39/datw/David/pisosphp/index.php?controller=pisosController.php&accion=rss',
            'http://192.168.1.60/miNombre/pisosphp/index.php?controller=pisosController.php&accion=rss'
            
        ];


        //Por cada fuente RSS creo un objeto de la clase rssModel
        
        $rss = new rssModel($urls);
            # code...
        

        $elem = $rss->elementos();

        //echo '<pre>';
        //print_r($elem);
        //echo '</pre>';

        //var_dump($rss->elementos());


        
        require('views/pisosRssExternoView.php');
        break;


    case 'listado':
    default:
        //Obtnemos el numero de paginas
        if(isset($_GET['numpag'])){
            $numpag =$_GET['numpag'];
        }
        else{
            $numpag = 0 ;
        }
        
        $num_regist_pag = 2;

        $numPag = $pisos->getNumPag($num_regist_pag);
        
        //Extraigo  el listado de pisos
        $elem = $pisos->listado($num_regist_pag,$numpag );

        //llamo a la vista 
        require('views/pisosView.php');
        
        break;
}


