<?php 

require 'models/class.noticiasModel.php';


$noticia = new NoticiasModel();

//Evaluamos la accion a realizar
if(isset($_GET['accion'])){
    $accion = $_GET['accion'];
}
else
{
    $accion = 'listado';
}

// Acciones a determinar
switch ($accion) {
    case 'detalle':
        $id=$_GET['id'];
        $e = $noticia->detalle($id);
        //llamo a la vista 
        require('views/noticiasDetalleView.php');
        break;
    case 'borrar':
        $id = $_GET['id'];
        $consult =$noticia->borrar($id);
        if($consult===true)
        {
            // return 'Insercion con exito';
            header('location:index.php?controller=noticiasController.php&accion=listado');
        }
        else
        {
            $error=$consulta;
            require 'views/pisosErrorView.php';
        }
        break;

    case 'insertar':
        require('views/noticiasAltaView.php');
        break;
    
    case 'insercion':
        $consult =$noticia->insercion(); 
        if($consult===true)
        {
            // return 'Insercion con exito';
            header('location:index.php?controller=noticiasController.php&accion=listado');
        }
        else
        {
            $error=$consulta;
            require 'views/pisosErrorView.php';
        }

        break;
            
    case 'modificar':
        $id=$_GET['id'];
        $e = $noticia->detalle($id);
        //llamo a la vista 
        require('views/noticiasModifView.php');
        break;

    case 'modifica':
        $consult =$noticia->modificacion(); 
        if($consult===true)
        {
            // return 'Insercion con exito';
            header('location:index.php?controller=noticiasController.php&accion=listado');
        }
        else
        {
            $error=$consulta;
            require 'views/pisosErrorView.php';
        }

        break;

    case 'listado':
    default:
        //Obtnemos el numero de paginas
        if(isset($_GET['numpag'])){
            $numpag =$_GET['numpag'];
        }
        else{
            $numpag = 0 ;
        }
        
        $num_regist_pag = 2;

        $numPag = $noticia->getNumPag($num_regist_pag);
        
        //Extraigo  el listado de pisos
        $elem = $noticia->listado($num_regist_pag,$numpag );

        //llamo a la vista 
        require('views/noticiasView.php');
        
        break;
}


