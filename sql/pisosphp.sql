-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 29-06-2017 a las 15:19:31
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pisosphp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `idNot` int(11) NOT NULL,
  `tituloNot` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `contenidoNot` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `autorNot` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `fechaNot` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`idNot`, `tituloNot`, `contenidoNot`, `autorNot`, `fechaNot`) VALUES
(1, 'Las hipotecas sobre viviendas suben un 14% en 2016', 'Según datos del Instituto Nacional de Estadística (INE), la concesión de hipotecas sobre viviendas ha subido un 14% en el conjunto de 2016 frente al año anterior, alcanzando las 281.328 firmas.', 'PepiloListo', '2017-06-25 16:28:00'),
(2, 'Cambio de tendencia en el mercado hipotecario', 'Durante las últimas seis semanas las novedades experimentadas por los bancos se han centrado en encarecer los tipos de sus hipotecas, afectando a 20 productos en total.', 'erBarcenAs', '2017-06-23 00:00:00'),
(3, 'El empleo y la escasez de oferta marcan el precio del alquiler', 'La recuperación del empleo es uno de los factores que determinan el auge del precio del alquiler en España. El cambio de tendencia es consecuencia de la mayor necesidad de movilidad laboral.', 'erBarcenAs', '2017-06-23 00:00:00'),
(4, 'El empleo y la escasez de oferta marcan el precio del alquiler', 'La recuperación del empleo es uno de los factores que determinan el auge del precio del alquiler en España. El cambio de tendencia es consecuencia de la mayor necesidad de movilidad laboral.', 'erBarcenAsII', '2017-06-23 00:00:00'),
(5, 'El TS pide aclarar los intereses de demora abusivos en los mercados bursatiles', 'El Tribunal Supremo tiene la intención de que la justicia europea determine si los intereses de demora de un préstamo deben considerarse desproporcionados para el cliente si estos son superiores al 2%.', 'leMontoro', '2017-06-26 18:06:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pisos`
--

CREATE TABLE `pisos` (
  `idPiso` int(11) NOT NULL,
  `direccionPiso` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `caracteristicasPiso` longtext COLLATE utf8_spanish_ci NOT NULL,
  `imagenPiso` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `precioPiso` float NOT NULL,
  `ciudadPiso` tinytext COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pisos`
--

INSERT INTO `pisos` (`idPiso`, `direccionPiso`, `caracteristicasPiso`, `imagenPiso`, `precioPiso`, `ciudadPiso`) VALUES
(1, 'Madrid 127. Las Margaritas-Universidad (Getafe)', 'Descripción de la promoción Residencial Madrid 127\r\n\r\nun concepto de viviendas que descubre una arquitectura moderna y funcional para que vivir en Residencial Madrid 127 se convierta en una experiencia única e irrepetible. Oficina y piso muestra en c/ Madrid 127, acceso desde c/ carabanchel 115 viviendas de 2, 3 y 4 dormitorios, con plaza de garaje y trastero. una urbanización de más de 4.000 m2 de zonas comunes. Un concepto arquitectónico innovador, donde la luminosidad, la calidad de los materiales y la excelente distribución de las estancias son sus señas de identidad. Una casa abierta al exterior en una de las mejores zonas de expansión de Getafe. En pleno campus de la Universidad Carlos III, frente al rectorado con la fachada principal dando a la calle Madrid. En una urbanización con todos los servicios para sentirte bien: piscina, pádel, zona deportiva, gimnasio, sala multiusos. Las viviendas se han proyectado con grandes salones y amplias terrazas. El edificio se construirá dotándolo de un sistema de ahorro de energía, para conseguir una buena calificación energética. los precios no incluyen iva. imágenes no contractuales y meramente ilustrativas sujetas a modificaciones por exigencias de orden técnico, jurídico o comercial de la dirección facultativa o autoridad competente', '1497968321piso1.jpg', 245000, 'Madrid'),
(2, 'Avda. San José 34. San José (Zaragoza Capital)', 'Descripción de la promoción Reina Sofia\r\n\r\nPróxima construcción, Edificio Reina Sofia, situado en la mejor zona de San José, frente a la Plaza Reina Sofía, a solo 10 minutos del centro de Zaragoza, zona completamente consolidada con todos los servicios. El edificio está compuesto de 20 viviendas distribuidas en pisos de 2,3 y 4 dormitorios más salón, impresionantes áticos con vistas a la avenida principal con plaza de garaje y trastero incluido en el precio. Calidades inmejorables, primeras marcas del mercado, fachadas ventiladas proyectadas con placas cerámicas, para conseguir el máximo aislamiento térmico-acústico, instalación de paneles solares en cubierta, cocinas completamente equipadas, aire acondicionado en toda la vivienda. Posibilidad de modificaciones en obra. Disponemos de plazas de garaje, trasteros y un amplio local de 300m2. Llámenos y le informaremos.', 'piso2.jpg', 205000, 'Zaragoza'),
(3, 'Avda. San José 34. San José (Zaragoza Capital)', 'Descripción de la promoción Reina Sofia\r\n\r\nPróxima construcción, Edificio Reina Sofia, situado en la mejor zona de San José, frente a la Plaza Reina Sofía, a solo 10 minutos del centro de Zaragoza, zona completamente consolidada con todos los servicios. El edificio está compuesto de 20 viviendas distribuidas en pisos de 2,3 y 4 dormitorios más salón, impresionantes áticos con vistas a la avenida principal con plaza de garaje y trastero incluido en el precio. Calidades inmejorables, primeras marcas del mercado, fachadas ventiladas proyectadas con placas cerámicas, para conseguir el máximo aislamiento térmico-acústico, instalación de paneles solares en cubierta, cocinas completamente equipadas, aire acondicionado en toda la vivienda. Posibilidad de modificaciones en obra. Disponemos de plazas de garaje, trasteros y un amplio local de 300m2. Llámenos y le informaremos.', 'piso2.jpg', 235000, 'Zaragoza'),
(5, 'Avda. San José 34. San José (Zaragoza Capital)', 'Descripción de la promoción Reina Sofia\r\n\r\nPróxima construcción, Edificio Reina Sofia, situado en la mejor zona de San José, frente a la Plaza Reina Sofía, a solo 10 minutos del centro de Zaragoza, zona completamente consolidada con todos los servicios. El edificio está compuesto de 20 viviendas distribuidas en pisos de 2,3 y 4 dormitorios más salón, impresionantes áticos con vistas a la avenida principal con plaza de garaje y trastero incluido en el precio. Calidades inmejorables, primeras marcas del mercado, fachadas ventiladas proyectadas con placas cerámicas, para conseguir el máximo aislamiento térmico-acústico, instalación de paneles solares en cubierta, cocinas completamente equipadas, aire acondicionado en toda la vivienda. Posibilidad de modificaciones en obra. Disponemos de plazas de garaje, trasteros y un amplio local de 300m2. Llámenos y le informaremos.', 'piso2.jpg', 205000, 'Zaragoza'),
(7, 'Plaza Ortilla / Ranillas', 'Tipo de inmueble : Piso\r\nPlanta : 6ª planta\r\nEstado : A reformar\r\nAntigüedad : De 30 a 50 años\r\nOrientación : Orientación Sur\r\nAgua caliente sanitaria : Gas Natural\r\nCalefacción : Gas Natural', 'piso3.jpg', 99000, 'Zaragoza'),
(8, 'Francisco Martinez Soria / Ranillas', 'Tipo de inmueble : Piso\r\nPlanta : 1ª planta\r\nEstado : Casi nuevo\r\nAntigüedad : De 5 a 10 años\r\nOrientación : Orientación Sureste', '1497965875piso3.jpg', 285000, 'Zaragoza');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsu` int(11) NOT NULL,
  `nombreUsu` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `claveUsu` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `correoUsu` tinytext COLLATE utf8_spanish_ci NOT NULL,
  `tipoUsu` tinytext COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsu`, `nombreUsu`, `claveUsu`, `correoUsu`, `tipoUsu`) VALUES
(1, 'admin', '81dc9bdb52d04dc20036dbd8313ed055', 'admin@gmail.com', 'admin'),
(2, 'HelloKytty', 'cd880b726e0a0dbd4237f10d15da46f4', 'kitty@gmail.com', 'normal'),
(3, 'Pepillo', '81dc9bdb52d04dc20036dbd8313ed055', 'pepe@gmail.com', 'normal'),
(4, 'Predrin', 'cd880b726e0a0dbd4237f10d15da46f4', 'pedrin@gmail.com', 'normal');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`idNot`);

--
-- Indices de la tabla `pisos`
--
ALTER TABLE `pisos`
  ADD PRIMARY KEY (`idPiso`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsu`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
  MODIFY `idNot` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `pisos`
--
ALTER TABLE `pisos`
  MODIFY `idPiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
